; Make Capslock esc
#InstallKeybdHook
SetCapsLockState, alwaysoff
Capslock::
Send {LControl Down}
KeyWait, CapsLock
Send {LControl Up}
if ( A_PriorKey = "CapsLock" )
{
    Send {Esc}
}
return

; Previous track 
Capslock & q::Media_Prev

; Next track 
Capslock & e::Media_Next

; Play/pause track 
Capslock & w::Media_Play_Pause

; Volume Up
Capslock & d::Volume_Up

; Volume Down
Capslock & s::Volume_Down

; Volume Mute
Capslock & a::Volume_Mute
